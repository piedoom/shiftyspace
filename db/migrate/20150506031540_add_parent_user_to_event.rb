class AddParentUserToEvent < ActiveRecord::Migration
  def change
    change_table :events do |t|
      t.integer :user_parent_id
    end
  end
end
