module ApplicationHelper
  def is_admin?
    if user_signed_in?
      if current_user.admin
        return true
      end
    end
  end

  def sign_up_button(event, *htmlclass)
    signedup = false
    event.users.each do |user|
      user.id == current_user.id ? signedup = true : nil
    end
    if user_signed_in?
      if signedup == false
        link_to "Sign Up for Event", "#{user_custom_update_path}?event=#{event.id}&signup=yes", class: "btn btn-default right #{htmlclass[0]}"
      else
        link_to "No Longer Attending", "#{user_custom_update_path}?event=#{event.id}&signup=no", class: "btn btn-danger right #{htmlclass[0]}"
      end
    end
  end

  def is_signed_up?(event)
    signedup = false
    event.users.each do |user|
      user.id == current_user.id ? signedup = true : nil
    end
    return signedup
  end

  def is_op_or_admin?(event)
    if current_user.admin == true
      return true
    elsif event.user == current_user
      return true
    end
  end
end
