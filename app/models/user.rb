class User < ActiveRecord::Base
  has_and_belongs_to_many :events
  has_many :createdEvents, :foreign_key => 'user_parent_id', :class_name => "Event"

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :confirmable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates_format_of :email,
      with: /(.)*\@rit\.edu/, message: 'must be RIT student email.'

end
