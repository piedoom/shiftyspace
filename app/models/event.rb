class Event < ActiveRecord::Base
  has_and_belongs_to_many :users
  belongs_to :category
  belongs_to :user, :foreign_key => :user_parent_id
  extend SimpleCalendar
  has_calendar :attribute => :datetime

  has_attached_file :image,
          :styles => { :original => "1920x500#",
                       :thumb => "1000x250#"},
          :convert_options => { :thumb => '-blur 0x16 -modulate 70,60' }

  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  def set_user!(user)
    self.user_parent_id = user.id
    self.save!
  end

end
