var initialize = function() {
    $.stellar( 'destroy' );
    $.stellar();
    $.stellar({ horizontalScrolling: false,
                verticalOffset: 0,
                horizontalOffset: 0
    });

}

$(document).ready( initialize );
$(document).on( 'page:load', initialize );