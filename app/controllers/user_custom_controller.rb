class UserCustomController < ApplicationController
  def update
    @event = Event.find(params[:event])
    if User.find(current_user.id).events.find_by_id(@event.id) == nil && params[:signup] == 'yes'
      @event.users << User.find(current_user.id)
    elsif User.find(current_user.id).events.find_by_id(@event.id) != nil && params[:signup] == 'no'
      @event.users.delete(User.find(current_user.id))
    end
    redirect_to @event
  end
end
