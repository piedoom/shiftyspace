class PagesController < ApplicationController
  def dashboard
    @myEvents = User.find(current_user.id).createdEvents
    @events = User.find(current_user.id).events
    @eventsFuture = User.find(current_user.id).events.where(["datetime >= ?", Date.today]).order("datetime ASC")
    @eventsPast = User.find(current_user.id).events.where(["datetime < ?", Date.today]).order("datetime ASC")
  end
end
