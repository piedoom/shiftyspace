class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def is_op_or_admin?(event)
    if current_user.admin == true
      return true
    elsif event.user == current_user
      return true
    end
  end
end
