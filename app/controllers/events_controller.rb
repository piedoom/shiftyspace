class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :secure

  # GET /events
  # GET /events.json
  def index
    if params[:filter] == nil
      @events = Event.where(["datetime >= ?", Date.today]).paginate(:page => params[:page], :per_page => 6).order("datetime ASC")
    elsif params[:filter] == 'onfloor'
      @events = Category.find(1).events.where(["datetime >= ?", Date.today]).paginate(:page => params[:page], :per_page => 30).order("datetime ASC")
    elsif params[:filter] == 'offfloor'
      @events = Category.find(2).events.where(["datetime >= ?", Date.today]).paginate(:page => params[:page], :per_page => 30).order("datetime ASC")
    elsif params[:filter] == 'all'
      @events = Event.all
    end
  end

  # GET /events/1
  # GET /events/1.json
  def show
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
    if is_op_or_admin?(@event) == false
      redirect_to events_path
    end
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
    @event.set_user!(current_user)

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    if is_op_or_admin?(@event)
      respond_to do |format|
        if @event.update(event_params)
          format.html { redirect_to @event, notice: 'Event was successfully updated.' }
          format.json { render :show, status: :ok, location: @event }
        else
          format.html { render :edit }
          format.json { render json: @event.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    if is_op_or_admin?(@event)
      @event.destroy
      respond_to do |format|
        format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    def secure
      redirect_to new_user_session_path unless user_signed_in?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:name, :description, :datetime, :category_id, :image)
    end
end
