json.array!(@events) do |event|
  json.extract! event, :id, :name, :description, :datetime
  json.url event_url(event, format: :json)
end
